# ansible-collection-playbook-poc

POC of using an ansible collection playbook

## Launch collection role
```
$ ansible-playbook test_role.yml
[WARNING]: Unable to parse /home/christophe/dev/clients/lydra/ansible-collection-playbook-poc/inventories as an inventory source
[WARNING]: No inventory was parsed, only implicit localhost is available
[WARNING]: provided hosts list is empty, only localhost is available. Note that the implicit localhost does not match 'all'

PLAY [localhost] *****************************************************************************************************************************************************************************

TASK [Gathering Facts] ***********************************************************************************************************************************************************************
ok: [localhost]

TASK [poc.playbooks.hello : Print Hello from role] *******************************************************************************************************************************************
ok: [localhost] => {
    "msg": "Hello world from poc.playbooks.hello"
}

PLAY RECAP ***********************************************************************************************************************************************************************************
localhost                  : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0  
```

## Launch collection playbook
```
$ ansible-playbook test_role.yml
[WARNING]: Unable to parse /home/christophe/dev/clients/lydra/ansible-collection-playbook-poc/inventories as an inventory source
[WARNING]: No inventory was parsed, only implicit localhost is available
[WARNING]: provided hosts list is empty, only localhost is available. Note that the implicit localhost does not match 'all'

PLAY [localhost] *****************************************************************************************************************************************************************************

TASK [Gathering Facts] ***********************************************************************************************************************************************************************
ok: [localhost]

TASK [poc.playbooks.hello : Print Hello from role] *******************************************************************************************************************************************
ok: [localhost] => {
    "msg": "Hello world from poc.playbooks.hello"
}

PLAY RECAP ***********************************************************************************************************************************************************************************
localhost                  : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0  
```
